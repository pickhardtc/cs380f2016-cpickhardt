#!/usr/bin/env python
# 13 Oct 2016
# Written by Oliver Bonham-Carter following the tutorial at
# http://sebastianraschka.com/Articles/2014_sqlite_in_python_tutorial.html
# email: obonhamcarter@allegheny.edu

# runs, "select * from instructor;" using python

import sqlite3

sqlite_file = "campus_ii.sqlite3" # the database file.

# Connecting to the database file
conn = sqlite3.connect(sqlite_file)
c = conn.cursor()


##LAB WORK BEGINS HERE

#############INSTRUCTOR TABLE
#1

table_name = 'instructor'   # name of the table to be queried
column1 = 'name'	    # attribute to query

c.execute('SELECT * FROM {tn} WHERE {cn}="Nelson" '.format(tn=table_name, cn=column1))
all_rows = c.fetchall()
print('1):', all_rows)


#2

table_name = 'instructor'   # name of the table to be queried
column1 = 'ID'	    # attribute to query
column2 = 'deptName'	    # attribute to search

c.execute('select {cn1} from {tn} where {cn2} = "compSci"'.format(tn=table_name, cn1=column1, cn2 = column2))
all_rows = c.fetchall()
print('2):', all_rows)


#3

table_name = 'instructor'   # name of the table to be queried
col1 = 'ID'	    # attribute to query
col2 = 'student'	        # attribute to query
col3 = 'deptName'	    #attribute to search

c.execute('SELECT {attrib1},{attrib2} FROM {tn} WHERE {attrib1}="compSci"'.format(attrib1=col1, attrib2=col2, tn=table_name, cn=col2))
all_rows = c.fetchall()
print('3):', all_rows)



#4

table_name = 'instructor'   #name of the table to be queried
column_1 = 'ID'   #attribute to query
column_2 = 'salary'   #attribute to query
column_3 = 'name'    #attribute to query

c.execute('SELECT * FROM {tn} WHERE {cn} like "M%"'.format(tn=table_name, cn=column_1))
all_rows = c.fetchall()
print('4):', all_rows)


#5

table_name = 'instructor'   #name of the table to be queried
column_1 = 'ID'   #attribute to query
column_2 = 'salary'   #attribute to query
column_3 = 'student'    #attribute to query

c.execute('SELECT * FROM {tn} WHERE {cn} like "_3"'.format(tn=table_name, cn=column_3))
all_rows = c.fetchall()
print('5):', all_rows)

#6

c.execute('SELECT * FROM {tn} WHERE {cn}="10108"'.format(tn=table_name, cn=column_1))
all_rows = c.fetchall()
print('6):', all_rows)

#7

table_name = 'instructor'   #name of the table to be queried
column_1 = 'ID'   #attribute to query
column_2 = 'name'   #attribute to query
column_3 = 'credits'    #attribute to query

c.execute('SELECT * FROM {tn} WHERE {cn} like "xS12"'.format(tn=table_name, cn=column_1))
all_rows = c.fetchall()
print('7):', all_rows)


#8

table_name = 'student'   #name of the table to be queried
column_1 = 'ID'   #attribute to query
column_2 = 'name'   #attribute to query
column_3 = 'credits'    #attribute to query

c.execute('UPDATE {tn} SET {cn1} == "S12" WHERE {cn2} == "Jameson"'.format(tn=table_name, cn1=column_1, cn2=column_2))
all_rows = c.fetchall()
print('8):', all_rows)


###########STUDENT TABLE


table_name = 'student'   #name of the table to be queried
column_1 = 'ID'   #attribute to query
column_2 = 'name'   #attribute to query
column_3 = 'totCred'    #attribute to query
column_4 = 'deptName'   #attribute to query

#1
c.execute('SELECT * FROM {tn} WHERE {cn}="S1" '.format(tn=table_name, cn=column_1))
all_rows = c.fetchall()
print('1):', all_rows)

#2
c.execute('select {cn1} from {tn} where {cn2} = "compSci"'.format(tn=table_name, cn1=column_2, cn2 = column_4))
all_rows = c.fetchall()
print('2):', all_rows)

#3
c.execute('SELECT {attrib1},{attrib2} FROM {tn} WHERE {attrib1}="3"'.format(attrib1=column_3, attrib2=column_2, tn=table_name))
all_rows = c.fetchall()
print('3):', all_rows)

#4
c.execute('SELECT * FROM {tn} WHERE {cn} like "%n"'.format(tn=table_name, cn=column_2))
all_rows = c.fetchall()
print('4):', all_rows)


#5
c.execute('SELECT * FROM {tn} WHERE {cn} like "xS__"'.format(tn=table_name, cn=column_2))
all_rows = c.fetchall()
print('5):', all_rows)

#6
c.execute('SELECT * FROM {tn} WHERE {cn}="Beuller"'.format(tn=table_name, cn=column_2))
all_rows = c.fetchall()
print('6):', all_rows)

#7
c.execute('SELECT * FROM {tn} WHERE {cn} like "xS12"'.format(tn=table_name, cn=column_1))
all_rows = c.fetchall()
print('7):', all_rows)


#8
c.execute('UPDATE {tn} SET {cn1} == "zS1111111" WHERE {cn2} == "Jameson"'.format(tn=table_name, cn1=column_1, cn2=column_2))
all_rows = c.fetchall()
print('8):', all_rows)



######COURSE TABLE
table_name = 'course'   # name of the table to be queried
col1 = 'courseId'	    # attribute to query
col2 = 'title'	        # attribute to query
col3 = 'deptName'
col4 = 'credits'	    #attribute to search
#1
c.execute('SELECT * FROM {tn} WHERE {cn}="CS100" '.format(tn=table_name, cn=col1))
all_rows = c.fetchall()
print('1):', all_rows)
#2
c.execute('select {cn1} from {tn} where {cn2} = "compSci"'.format(tn=table_name, cn1=col1, cn2 = col3))
all_rows = c.fetchall()
print('2):', all_rows)
#3
c.execute('SELECT {attrib1},{attrib2} FROM {tn} WHERE {attrib3}="3"'.format(attrib1=col1, attrib2=col2, tn=table_name, attrib3 = col4))
all_rows = c.fetchall()
print('3):', all_rows)
#4
c.execute('SELECT * FROM {tn} WHERE {cn} like "Bio%"'.format(tn=table_name, cn=col2))
all_rows = c.fetchall()
print('4):', all_rows)
#5
c.execute('SELECT * FROM {tn} WHERE {cn} like "___00"'.format(tn=table_name, cn=col1))
all_rows = c.fetchall()
print('5):', all_rows)
#6
c.execute('SELECT * FROM {tn} WHERE {cn}="CS605"'.format(tn=table_name, cn=col1))
all_rows = c.fetchall()
print('6):', all_rows)
#7
c.execute('SELECT * FROM {tn} WHERE {cn} like "%i%"'.format(tn=table_name, cn=col2))
all_rows = c.fetchall()
print('7):', all_rows)
#8
c.execute('UPDATE {tn} SET {cn1} == "CSWins" WHERE {cn2} == "Bioinformatics_1"'.format(tn=table_name, cn1=col3, cn2=col2))
all_rows = c.fetchall()
print('8):', all_rows)

############DEPARTMENT TABLE
table_name = 'department'   # name of the table to be queried
col1 = 'courseId'	    # attribute to query
col2 = 'courseType'	    # attribute to search
col3 = 'deptName'	    # attribute to search
#1
c.execute('SELECT * FROM {tn} WHERE {cn}="Math" '.format(tn=table_name, cn=col3))
all_rows = c.fetchall()
print('1):', all_rows)
#2
c.execute('select {cn1} from {tn} where {cn2} = "compSci"'.format(tn=table_name, cn1=col1, cn2 = col3))
all_rows = c.fetchall()
print('2):', all_rows)
#3
c.execute('SELECT {attrib1},{attrib2} FROM {tn} WHERE {cn}="biology"'.format(attrib1=col1, attrib2=col2, tn=table_name, cn=col3))
all_rows = c.fetchall()
print('3):', all_rows)
#4
c.execute('SELECT * FROM {tn} WHERE {cn} like "BIO%"'.format(tn=table_name, cn=col1))
all_rows = c.fetchall()
print('4):', all_rows)
#5
c.execute('SELECT * FROM {tn} WHERE {cn} like "___101"'.format(tn=table_name, cn=col1))
all_rows = c.fetchall()
print('5):', all_rows)
#6
c.execute('SELECT * FROM {tn} WHERE {cn}="MTH202"'.format(tn=table_name, cn=col1))
all_rows = c.fetchall()
print('6):', all_rows)
#7
c.execute('SELECT * FROM {tn} WHERE {cn} like "Math"'.format(tn=table_name, cn=col3))
all_rows = c.fetchall()
print('7):', all_rows)
#8
c.execute('UPDATE {tn} SET {cn1} == "fun" WHERE {cn2} == "Math"'.format(tn=table_name, cn1=col2, cn2=col3))
all_rows = c.fetchall()
print('8):', all_rows)

############TAKES TABLE
table_name = 'takes'   #name of the table to be queried
col1 = 'ID'   #attribute to query
col2 = 'courseID'   #attribute to query
col3 = 'secId'    #attribute to query
col4 = 'semester'   #attribute to query
col5 = 'year'   #attribute to query
col6 = 'grade'     #attribute to query

#1
c.execute('SELECT * FROM {tn} WHERE {cn}="1" '.format(tn=table_name, cn=col4))
all_rows = c.fetchall()
print('1):', all_rows)
#2
c.execute('select {cn1} from {tn} where {cn2} = "2009"'.format(tn=table_name, cn1=col1, cn2 = col5))
all_rows = c.fetchall()
print('2):', all_rows)
#3
c.execute('SELECT {attrib1},{attrib2} FROM {tn} WHERE {cn}="A"'.format(attrib1=col1, attrib2=col2, tn=table_name, cn=col6))
all_rows = c.fetchall()
print('3):', all_rows)
#4
c.execute('SELECT * FROM {tn} WHERE {cn} like "45%"'.format(tn=table_name, cn=col1))
all_rows = c.fetchall()
print('4):', all_rows)
#5
c.execute('SELECT * FROM {tn} WHERE {cn} like "BIO___"'.format(tn=table_name, cn=col2))
all_rows = c.fetchall()
print('5):', all_rows)
#6
c.execute('SELECT * FROM {tn} WHERE {cn}="31114"'.format(tn=table_name, cn=col1))
all_rows = c.fetchall()
print('6):', all_rows)
#7
c.execute('SELECT * FROM {tn} WHERE {cn} like "1"'.format(tn=table_name, cn=col3))
all_rows = c.fetchall()
print('7):', all_rows)
#8
c.execute('UPDATE {tn} SET {cn1} == "Fall" WHERE {cn2} == "A"'.format(tn=table_name, cn1=col4, cn2=col6))
all_rows = c.fetchall()
print('8):', all_rows)

################TEACHES TABLE

table_name = 'teaches'   #name of the table to be queried
col1 = 'ID'   #attribute to query
col2 = 'courseId'   #attribute to query
col3 = 'secID'    #attribute to query
col4 = 'semester'   #attribute to query
col5 = 'year'   #attribute to query
#1
c.execute('SELECT * FROM {tn} WHERE {cn}="Summer" '.format(tn=table_name, cn=col4))
all_rows = c.fetchall()
print('1):', all_rows)
#2
c.execute('select {cn1} from {tn} where {cn2} = "2016"'.format(tn=table_name, cn1=column1, cn2 = col5))
all_rows = c.fetchall()
print('2):', all_rows)
#3
c.execute('SELECT {attrib1},{attrib2} FROM {tn} WHERE {cn}="1"'.format(attrib1=col1, attrib2=col2, tn=table_name, cn=col3))
all_rows = c.fetchall()
print('3):', all_rows)
#4
c.execute('SELECT * FROM {tn} WHERE {cn} like "Eng%"'.format(tn=table_name, cn=col2))
all_rows = c.fetchall()
print('4):', all_rows)
#5
c.execute('SELECT * FROM {tn} WHERE {cn} like "1234___"'.format(tn=table_name, cn=col1))
all_rows = c.fetchall()
print('5):', all_rows)
#6
c.execute('SELECT * FROM {tn} WHERE {cn}="12359"'.format(tn=table_name, cn=col1))
all_rows = c.fetchall()
print('6):', all_rows)
#7
c.execute('SELECT * FROM {tn} WHERE {cn} like "S%"'.format(tn=table_name, cn=col4))
all_rows = c.fetchall()
print('7):', all_rows)
#8
c.execute('UPDATE {tn} SET {cn1} == "FUNFUNFUN" WHERE {cn2} == "1"'.format(tn=table_name, cn1=col2, cn2=col3))
all_rows = c.fetchall()
print('8):', all_rows)

# Committing changes and closing the connection to the database file
conn.commit()


# Closing the connection to the database file
conn.close()


