from __future__ import unicode_literals
from django.db import models

class Movie(models.Model):
    actor = models.CharField(max_length = 250) # holds the name of max length 250 chars
    film_title = models.CharField(max_length = 500) # holds album name
    genre = models.CharField(max_length = 100) #
    director = models.CharField(max_length = 250)
#end of class Movie()
# add a string representation of an object's attributes
    def __str__(self):
        return self.video_title + " - " + self.actor

# foreign keys link the songs to a particular album.
#class Actor(models.Model): #links the songs to the album class
# when you delete an album, remove the its associated songs, as well.
#    movie = models.ForeignKey(video, on_delete=models.CASCADE)
#    file_type = models.CharField(max_length = 10) # holds the type of file containing music
#   song_title = models.CharField(max_length = 250) # holds the song title.
#
## end of class Actor ()
