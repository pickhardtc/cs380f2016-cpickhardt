#!/usr/bin/python

import sqlite3
conn = sqlite3.connect('pokemon.sqlite3')
print "Opened database successfully";

# Insert
conn.execute("INSERT INTO pokemon() VALUES (10091, 'OBC-mon', 1000)")
conn.execute("INSERT INTO pokemon() VALUES (10092, 'JJ-mon', 1001)")
conn.execute("INSERT INTO pokemonInfo() VALUES (10091, 30, 100, 230, 0)")
conn.execute("INSERT INTO pokemonInfo() VALUES (10092, 27, 90, 370, 0)")
conn.commit()
print "Records created successfully!"

# Delete
conn.execute("DELETE from pokemon where ID = 5;")
conn.commit()
print "Row deleted successfully!"

# Update
conn.execute("UPDATE pokemon set species-id = 1005 where identifier == 'OBC-mon'")
conn.execute("UPDATE pokemonInfo set height = 50 where id_info == 10091")
conn.commit()
print "Updates successful!"

#Select
cursor = conn.execute("SELECT identifier FROM pokemon")
for row in cursor:
    print "Name: ", row[0], "\n"
cursor = conn.execute("SELECT id, identifier FROM pokemon")
for row in cursor:
    print "ID: ", row[0]
    print "Name: ", row[1], "\n"
cursor = conn.execute("SELECT id_info FROM pokemonInfo")
for row in cursor:
    print "ID: ", row[0], "\n"
cursor = conn.execute("SELECT height, weight FROM pokemonInfo")
for row in cursor:
    print "Height: ", row[0]
    print "Weight: ", row[1], "\n"
cursor = conn.execute("SELECT base_experience FROM pokemonInfo")
for row in cursor:
    print "Base experience: ", row[0]. "\n"
cursor = conn.execute("SELECT id, identifier, species_id FROM pokemon where identifier =='charizard'")
for row in cursor:
    print "ID: ", row[0]
    print "Name: ", row[1]
    print "Species ID: ", row[2], "\n"
cursor = conn.execute("SELECT id FROM pokemon where species_id == 27")
for row in cursor:
    print "id: ", row[0], "\n"
cursor = conn.execute("SELECT * from pokemon")
for row in cursor:
    print "id: ", row[0]
    print "name: ", row[1]
    print "species id: ", row[2], "\n"
cursor = conn.execute("SELECT * FROM pokemonInfo")
for row in cursor:
    print "id: ", row[0]
    print "height: ", row[1]
    print "weight: ",row[2]
    print "base experience: ", row[3]
    print "default?: ", row[4], "\n"
conn.commit()
print "Select statements successful!"
conn.close()
