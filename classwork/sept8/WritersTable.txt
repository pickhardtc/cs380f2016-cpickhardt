CREATE TABLE Writers (
       id                INTEGER NOT NULL PRIMARY KEY,
       first_name        VARCHAR(15) NOT NULL,
       middle_name       VARCHAR(15),
       last_name         VARCHAR(15) NOT NULL,
       birth_date        VARCHAR(10) NOT NULL,
       death_date        VARCHAR(10),
       country_of_origin VARCHAR(20) NOT NULL
);
    
CREATE TABLE OtherWriters (
       id                INTEGER NOT NULL PRIMARY KEY,
       first_name        VARCHAR(15) NOT NULL,
       middle_name       VARCHAR(15),
       last_name         VARCHAR(15) NOT NULL,
       birth_date        VARCHAR(10) NOT NULL,
       death_date        VARCHAR(10),
       country_of_origin VARCHAR(20) NOT NULL
);

INSERT into Writers VALUES(1, 'Ezra', 'Weston
Loomis', 'Pound', '30/10/1885', '1/11/1972', 'USA');

INSERT into Writers VALUES(2, 'Arthor', 'Conan',
'Doyle', '05/22/1859', '07/7/1930', 'UK');

INSERT into Writers VALUES(3, 'Ernest', 'Miller',
'Hemingway', '07/21/1899', '07/02/1961', 'USA');

INSERT into Writers VALUES(4, 'Edward',
'Montgomery', 'Wlliam III', '07/21/1860',
'07/02/1940', 'UK');

CREATE TABLE Pokemon (
       primary_key          INTEGER NOT NULL PRIMARY KEY,
       name                 VARCHAR(15) NOT NULL,
       attack               VARCHAR(15),
       type                 VARCHAR(15) NOT NULL,
       color                VARCHAR(10) NOT NULL,
       movement             VARCHAR(10) NOT NULL
);

INSERT into Pokemon VALUES(1,'Bulbasaur', 'Overgrow', 'Grass', 'Green', 'walk');
INSERT into Pokemon VALUES(2,'Ivysaur', 'Overgrow', 'Grass', 'Green', 'walk');
INSERT into Pokemon VALUES(3,'Venusaur', 'Overgrow', 'Grass', 'Green', 'walk');
INSERT into Pokemon VALUES(4,'Charmander', 'Blaze', 'Fire', 'Orange', 'walk');
INSERT into Pokemon VALUES(5,'Charmeleon', 'Blaze', 'Fire', 'Orange', 'walk');
INSERT into Pokemon VALUES(6,'Charizard', 'Blaze', 'Fire/Flying', 'Orange', 'walk');




