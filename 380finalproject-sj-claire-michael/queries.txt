/study/participant[major != 'Computer Science'][minor != Computer Science]/comfort/text()
This query will show the comfort level of people who are not experienced with computer science(non majors or minors)

/study/participant[major = 'Computer Science'][minor = Computer Science]/comfort/text()
This query shows the comfort level of people how are computer science majors or minors

/study/participant[major != 'Computer Science'][minor != Computer Science]/feels/text()
This query will show the feelings of people who are not experienced with computer science(non majors or minors)

/study/participant[major = 'Computer Science'][minor = Computer Science]/feels/text()
This query will show the feelings of people who are  experienced with computer science(non majors or minors)

/study/participant[major != 'Computer Science'][minor != Computer Science]/ai/text()
This query will show the opinion of AI of people who are not experienced with computer science(non majors or minors)

/study/participant[major = 'Computer Science'][minor = Computer Science]/ai/text()
This query will show the opinion of AI of people who are experienced with computer science(non majors or minors)

/study/participant[major = 'Computer Science'][minor = Computer Science]/opinions/text()
This query will show the opinions of computer science of people who are experienced with computer science(non majors or minors)

/study/participant[major != 'Computer Science'][minor != Computer Science]/opinions/text()
This query will show the opinions of computer science of people who are not experienced with computer science(non majors or minors)

/study/participant[graduate = 'No'][major != 'Computer Science']/comfort/text()
This query show the comfort level of computer science of people who have graduated from college and where not computer science majors

/study/participant[graduate != 'No']/comfort/text()
This query shows the comfort level of people who have not graduated

/study/participant[graduate = 'No']/feels/text()
This query shows the feeling towards computer science of people who have not graduated

/study/participant[graduate != 'No']/feels/text()
This query shows the feeling towards computer science of people that have graduated.
